from blog.models import ArticleModel
from django.contrib import admin


# Register your models here.
@admin.register(ArticleModel)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ("title", "author", "created_at")
